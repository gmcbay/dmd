package dmd

// DMD4000 control library
// Go version by George McBay (george.mcbay@gmail.com)
// Adapted from libD4000 (https://github.com/grinner/Polonator/tree/master/polonator/illum/libD4000)
//
// libD4000:
//   Formerly D4000_usb.cpp and DDC4000Ctl.cpp
//   Original copyright by Texas Instruments
//   Ported to Linux (and other *nix) via libusb0.1 with permission from TI
//   by Dr. Daniel Levner, Church Lab, Harvard Medical School
//   Copyright Daniel Levner & Texas Instruments, Inc. (c) 2009
//

// Currently only amd64 is supported as a link target.  Other
// architectures with libusb should work fine but they are untested
// and will require a proper build of the libusb library

/*
#cgo amd64 LDFLAGS: -L. -lusb_amd64
#include "lusb0_usb.h"
*/
import "C"

import (
	"fmt"
	"log"
	"time"
	"unsafe"
)

const (
	// used to detect first stage dmd power-on state.  If dmd is in this state,
	// we'll call fxload to bootstrap the device
	cypressVendorId  = 0x04b4
	cypressProductId = 0x8613

	vendorId  = 0x0451
	productId = 0xaf31

	Type1080P = 0
	TypeXGA   = 1

	AllBlocks = 16

	// code embedded in the FPGA firmware to designate a D4000
	versionCodeD4000 = 0xAB020000
	// In bytes, should be a multiple of 512
	maxTransferSize = 63488

	usbTimeout = 1000

	usbEndPointBulkOut  = 0x2
	usbEndPointBulkIn   = 0x06
	usbEndPointFpgaProg = 0x8

	// D4000 USB Register Adresses
	addrTypeDMD       = 0x0010
	addrVersionDDC    = 0x0011
	addrBlkMd         = 0x0017
	addrBlkAd         = 0x0018
	addrRowMd         = 0x0014
	addrRowAd         = 0x0015
	addrCtl1          = 0x0003
	addrCtl2          = 0x0016
	addrGPIO          = 0x0019
	addrNumRowLoads   = 0x0020
	addrResetComplete = 0x0021
	addrGpioResetFlag = 0x0022

	// D4000 USB Control register 0x03 bits
	ctlBitWriteBlock = 0x0001
	ctlBitResetCmlpt = 0x0008
	ctlBitClearFifos = 0x0010

	// D4000 USB Control register 0x16 bits
	ctlBitStepVcc   = 0x0001
	ctlBitCompData  = 0x0002
	ctlBitNsFlip    = 0x0004
	ctlBitWdt       = 0x0008
	ctlBitPwrFloatz = 0x0010
	ctlBitExtReset  = 0x0020
	ctlBitRst2Blkz  = 0x0040

	// D4000 Block Modes
	blkMdNoop   = 0
	blkMdClBlk  = 1
	blkMdRstBlk = 2
	blkMd11     = 3

	// D4000 Row Modes
	rowMdNoop   = 0
	rowMdInc    = 1
	rowMdSet    = 2
	rowMdSetPnt = 3
)

type Device struct {
	dev           *C.struct_usb_device
	handle        *C.struct_dev_handle
	dmdType       uint16
	width         int
	height        int
	totalBlocks   int
	rowsPerBlock  int
	bytesPerRow   int
	sizeinBytes   int
	blocksPerLoad int
}

var devices []*Device

func findInterfaceId(dev *C.struct_usb_device) (interfaceId C.int) {
	if dev != nil && dev.config != nil && dev.config._interface != nil &&
		dev.config._interface.altsetting != nil {
		interfaceId = C.int(dev.config._interface.altsetting.bInterfaceNumber)
	}

	return
}

func Initialize() (err error) {
	log.Printf("dmd: initializing...\n")

	devices = make([]*Device, 0)

	C.usb_init()
	C.usb_find_busses()
	busHead := C.usb_get_busses()

	if C.usb_find_devices() < 1 {
		// no devices found
		err = fmt.Errorf("dmd: No dmd devices found.\n")
		return
	}

	rescan := false

	// stage 0 (any Cypress EZUSB devices we need to bootstrap?)
	for bus := busHead; bus != nil; bus = bus.next {
		for dev := bus.devices; dev != nil; dev = dev.next {
			log.Printf("dmd: usb dev: v: 0x%x, p: 0x%x\n", dev.descriptor.idVendor, dev.descriptor.idProduct)

			if dev.descriptor.idVendor == cypressVendorId &&
				dev.descriptor.idProduct == cypressProductId {
				devHandle := C.usb_open(dev)

				log.Printf("dmd: initializing Cypress usb device\n")

				if err = initCypressFx2(devHandle); err != nil {
					return
				}

				rescan = true

				time.Sleep(2 * time.Second)
			}
		}
	}

	if rescan {
		C.usb_init()
		C.usb_find_busses()
		busHead = C.usb_get_busses()

		if C.usb_find_devices() < 1 {
			// no devices found
			err = fmt.Errorf("dmd: No dmd devices found.\n")
			return
		}
	}

	// stage 1, DMD (if available) should be bootstrapped
	for bus := busHead; bus != nil; bus = bus.next {
		for dev := bus.devices; dev != nil; dev = dev.next {
			if dev.descriptor.idVendor == vendorId && dev.descriptor.idProduct == productId {
				devHandle := C.usb_open(dev)

				if devHandle == nil {
					err = fmt.Errorf("dmd: Unable to open dmd device: %v\n", dev)
					return
				}

				device := &Device{dev: dev, handle: devHandle}

				if res := C.usb_claim_interface(devHandle, C.int(findInterfaceId(dev))); res != 0 {
					err = fmt.Errorf("dmd: Unable to claim interface for device: %v\n", dev)
					return
				}

				devices = append(devices, device)
			}
		}
	}

	log.Printf("dmd: %v devices located...\n", len(devices))

	return
}

func Destroy() {
	for i := 0; i < len(devices); i++ {
		C.usb_release_interface(devices[i].handle, findInterfaceId(devices[i].dev))
		C.usb_close(devices[i].handle)
	}

	devices = nil
}

func NumDev() int {
	if devices == nil {
		return 0
	}

	return len(devices)
}

func (dev *Device) FPGARev() (uint32, error) {
	var a, b uint16
	var err error

	if a, err = dev.registerRead(0); err != nil {
		return 0, err
	}

	if b, err = dev.registerRead(1); err != nil {
		return 0, err
	}

	return ((uint32(a) << 16) & 0xFFFF0000) + uint32(b), nil
}

func ConnectDevice(devNum int16) (dev *Device, err error) {
	dev = devices[devNum]

	var fpgaRev uint32

	if fpgaRev, err = dev.FPGARev(); err != nil {
		return
	}

	if (fpgaRev & 0xFFFF0000) != versionCodeD4000 {
		if err = dev.downloadAppsFPGACode(); err != nil {
			return
		}
	}

	if dev.dmdType, err = dev.registerRead(addrTypeDMD); err != nil {
		return
	}

	if dev.dmdType == Type1080P {
		dev.width = 1920
		dev.height = 1080
		dev.totalBlocks = 15
		dev.rowsPerBlock = 72
		dev.bytesPerRow = 240
		dev.sizeinBytes = 259200
		dev.blocksPerLoad = 2
	} else {
		dev.width = 1024
		dev.height = 768
		dev.totalBlocks = 16
		dev.rowsPerBlock = 48
		dev.bytesPerRow = 128
		dev.sizeinBytes = 98304
		dev.blocksPerLoad = 5
	}

	return
}

func (dev *Device) DMDType() uint16 {
	return dev.dmdType
}

func (dev *Device) Width() int {
	return dev.width
}

func (dev *Device) Height() int {
	return dev.height
}

func (dev *Device) TotalBlocks() int {
	return dev.totalBlocks
}

func (dev *Device) RowsPerBlock() int {
	return dev.rowsPerBlock
}

func (dev *Device) BytesPerRow() int {
	return dev.bytesPerRow
}

func (dev *Device) SizeInBytes() int {
	return dev.sizeinBytes
}

func (dev *Device) vendorRequestIn(request byte) (buf [2]byte, err error) {
	if result := C.usb_control_msg(dev.handle,
		C.USB_RECIP_DEVICE|C.USB_TYPE_VENDOR|C.USB_ENDPOINT_IN,
		C.int(request),
		0x0000,
		0x0000,
		(*C.char)(unsafe.Pointer(&buf[0])),
		2,
		usbTimeout); result < 0 {
		err = fmt.Errorf("dmd: Failure in vendorRequestIn: %v\n", result)
	}

	return
}

func (dev *Device) vendorRequestOut(request byte) (err error) {
	var buf [2]byte

	if result := C.usb_control_msg(dev.handle,
		C.USB_RECIP_DEVICE|C.USB_TYPE_VENDOR|C.USB_ENDPOINT_OUT,
		C.int(request),
		0x0000,
		0x0000,
		(*C.char)(unsafe.Pointer(&buf[0])),
		2,
		usbTimeout); result < 0 {
		err = fmt.Errorf("dmd: Failure in vendorRequestOut: %v\n", result)
	}

	return
}

func (dev *Device) downloadAppsFPGACode() (err error) {
	log.Printf("dmd: downloadAppsFPGACode...\n")

	// Send code 0xBB to start FPGA program
	dev.vendorRequestOut(0xBB)

	if err = initFpgaFirmware(dev.handle); err != nil {
		return
	}

	// Wait for 0xBC01 to come back from 0xBC request to signify firmware received
	// Only try a couple times, timeout after
	for c := 0; c < 10; c++ {
		var buf [2]byte

		if buf, err = dev.vendorRequestIn(0xBC); err != nil {
			err = fmt.Errorf("dmd: Unable to program FPGA firmware [request in error]: %v\n", err)
			return
		}

		if buf[0] == 0xBC && buf[1] == 0x01 {
			// Success
			return
		} else {
			// Wait a little bit before requesting again
			time.Sleep(200 * time.Millisecond)
		}
	}

	err = fmt.Errorf("dmd: Unable to program FPGA firmware [timeout on tries]\n")

	return
}

func (dev *Device) registerRead(reg C.int) (value uint16, err error) {
	var buf [2]byte

	if result := C.usb_control_msg(dev.handle,
		C.USB_RECIP_DEVICE|C.USB_TYPE_VENDOR|C.USB_ENDPOINT_IN,
		0xBA,
		reg,
		0x0000,
		(*C.char)(unsafe.Pointer(&buf[0])),
		2,
		usbTimeout); result < 0 {
		err = fmt.Errorf("dmd: Failure in registerRead: %v\n", result)
		return
	}

	for i := 0; i < 2; i++ {
		value = (value << 8) | uint16(buf[i])
	}

	return
}

func (dev *Device) registerWrite(reg C.int, data uint16) (err error) {
	var buf [2]byte

	for i := 1; i >= 0; i-- {
		buf[i] = byte(data % 256)
		data /= 256
	}

	if result := C.usb_control_msg(dev.handle,
		C.USB_RECIP_DEVICE|C.USB_TYPE_VENDOR|C.USB_ENDPOINT_OUT,
		0xBA,
		reg,
		0x0000,
		(*C.char)(unsafe.Pointer(&buf[0])),
		2,
		usbTimeout); result < 0 {
		err = fmt.Errorf("dmd: Failure in registerWrite: %v\n", result)
	}

	return
}

func (dev *Device) LoadControl() (value int16, err error) {
	if err = dev.registerWrite(addrNumRowLoads, ctlBitWriteBlock); err != nil {
		return
	}

	err = dev.registerWrite(addrCtl1, ctlBitWriteBlock)

	return
}

func (dev *Device) repeatLoadControl(count int) (err error) {
	for lc := 0; lc < count; lc++ {
		if _, err = dev.LoadControl(); err != nil {
			return
		}
	}

	return
}

func (dev *Device) RowMd() (uint16, error) {
	return dev.registerRead(addrRowMd)
}

func (dev *Device) SetRowMd(value uint16) error {
	return dev.registerWrite(addrRowMd, value)
}

func (dev *Device) RowAddr() (uint16, error) {
	return dev.registerRead(addrRowAd)
}

func (dev *Device) SetRowAddr(value uint16) error {
	return dev.registerWrite(addrRowAd, value)
}

func (dev *Device) BlkMd() (uint16, error) {
	return dev.registerRead(addrBlkMd)
}

func (dev *Device) SetBlkMd(value uint16) error {
	return dev.registerWrite(addrBlkMd, value)
}

func (dev *Device) BlkAd() (uint16, error) {
	return dev.registerRead(addrBlkAd)
}

func (dev *Device) SetBlkAd(value uint16) error {
	return dev.registerWrite(addrBlkAd, value)
}

func (dev *Device) GPIO() (uint16, error) {
	return dev.registerRead(addrGPIO)
}

func (dev *Device) SetGPIO(value uint16) error {
	return dev.registerWrite(addrGPIO, value)
}

func (dev *Device) addrCtl2Value(mask uint16) (bool, error) {
	var reg uint16
	var err error

	if reg, err = dev.registerRead(addrCtl2); err != nil {
		return false, err
	}

	return (reg & ctlBitExtReset) > 0, nil
}

func (dev *Device) setAddrCtl2Value(mask int16, value bool) (err error) {
	var currentValue uint16

	if currentValue, err = dev.registerRead(addrCtl2); err != nil {
		return
	}

	newValue := int16(currentValue)

	if value {
		newValue |= mask
	} else {
		newValue &= ^mask
	}

	err = dev.registerWrite(addrCtl2, uint16(newValue))

	return
}

func (dev *Device) PwrFloat() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitPwrFloatz)
}

func (dev *Device) SetPwrFloat(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitPwrFloatz, value)
}

func (dev *Device) WDT() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitWdt)
}

func (dev *Device) SetWDT(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitWdt, value)
}

func (dev *Device) NSFlip() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitNsFlip)
}

func (dev *Device) SetNSFlip(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitNsFlip, value)
}

func (dev *Device) CompData() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitCompData)
}

func (dev *Device) SetCompData(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitCompData, value)
}

func (dev *Device) StepVCC() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitStepVcc)
}

func (dev *Device) SetStepVCC(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitStepVcc, value)
}

func (dev *Device) Rst2Blkz() (result bool, err error) {
	return dev.addrCtl2Value(ctlBitRst2Blkz)
}

func (dev *Device) SetRst2Blkz(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitRst2Blkz, value)
}

func (dev *Device) ExtResetEnbl() (bool, error) {
	return dev.addrCtl2Value(ctlBitExtReset)
}

func (dev *Device) SetExtResetEnbl(value bool) (err error) {
	return dev.setAddrCtl2Value(ctlBitExtReset, value)
}

func (dev *Device) SetGPIOResetComplete() error {
	return dev.registerWrite(addrGpioResetFlag, 1)
}

func (dev *Device) VersionDDC() (uint16, error) {
	return dev.registerRead(addrVersionDDC)
}

func (dev *Device) Descriptor() (desc [14]int, err error) {
	if dev.dev == nil {
		err = fmt.Errorf("dmd: Requested descriptor for invalid device: %v\n", dev)
		return
	}

	desc[0] = int(dev.dev.descriptor.bLength)
	desc[1] = int(dev.dev.descriptor.bDescriptorType)
	desc[2] = int(dev.dev.descriptor.bcdUSB)
	desc[3] = int(dev.dev.descriptor.bDeviceClass)
	desc[4] = int(dev.dev.descriptor.bDeviceSubClass)
	desc[5] = int(dev.dev.descriptor.bDeviceProtocol)
	desc[6] = int(dev.dev.descriptor.bMaxPacketSize0)
	desc[7] = int(dev.dev.descriptor.idVendor)
	desc[8] = int(dev.dev.descriptor.idProduct)
	desc[9] = int(dev.dev.descriptor.bcdDevice)
	desc[10] = int(dev.dev.descriptor.iManufacturer)
	desc[11] = int(dev.dev.descriptor.iProduct)
	desc[12] = int(dev.dev.descriptor.iSerialNumber)
	desc[13] = int(dev.dev.descriptor.bNumConfigurations)

	return
}

func (dev *Device) FirmwareRev() (result int32, err error) {
	var buf [2]byte

	if buf, err = dev.vendorRequestIn(0xBD); err != nil {
		err = fmt.Errorf("dmd: Unable to get FPGA firmware rev: %v\n", err)
		return
	}

	result = ((int32(buf[0]) << 8) & 0xFF00) + int32(buf[1])

	return
}

func (dev *Device) ClearFifos() error {
	return dev.registerWrite(addrCtl1, ctlBitClearFifos)
}

func (dev *Device) FloatMirrors() (err error) {
	if err = dev.SetRowMd(rowMdNoop); err != nil {
		return
	}

	if err = dev.SetBlkAd(12); err != nil {
		return
	}

	if err = dev.SetBlkMd(blkMd11); err != nil {
		return
	}

	if _, err = dev.LoadControl(); err != nil {
		return
	}

	time.Sleep(1 * time.Millisecond)

	if err = dev.SetBlkMd(blkMdNoop); err != nil {
		return
	}

	if err = dev.repeatLoadControl(4); err != nil {
		return
	}

	return
}

func (dev *Device) dataWrite(writeBuffer []byte) (err error) {
	writeSize := len(writeBuffer)

	if writeSize%512 != 0 {
		paddedBuffer := make([]byte, (len(writeBuffer)/512)*512+512)
		copy(paddedBuffer, writeBuffer)
		writeBuffer = paddedBuffer
	}

	if result := C.usb_bulk_write(dev.handle,
		usbEndPointBulkOut|C.USB_ENDPOINT_OUT,
		(*C.char)(unsafe.Pointer(&writeBuffer[0])),
		C.int(len(writeBuffer)),
		usbTimeout); result < 0 {
		err = fmt.Errorf("dmd: Failure in dataWrite: %v\n", result)
	}

	return
}

func (dev *Device) LoadData(rowData []byte) (err error) {
	var numberOfRows int

	numberOfRows = len(rowData) / dev.bytesPerRow

	if err = dev.ClearFifos(); err != nil {
		return
	}

	if err = dev.dataWrite(rowData); err != nil {
		return
	}

	if err = dev.registerWrite(addrNumRowLoads, uint16(numberOfRows)); err != nil {
		return
	}

	err = dev.registerWrite(addrCtl1, ctlBitWriteBlock)

	return
}

// This uses block numbers indexed from 0! The TI C/C++ API uses indexes starting from 1
func (dev *Device) Clear(blockNum int, doReset bool) (err error) {
	if err = dev.SetBlkMd(blkMdClBlk); err != nil {
		return
	}

	if err = dev.SetRowMd(rowMdNoop); err != nil {
		return
	}

	if err = dev.SetRowAddr(0); err != nil {
		return
	}

	if blockNum <= 15 {
		// Clear individual block specified
		if err = dev.SetBlkAd(uint16(blockNum)); err != nil {
			return
		}

		if _, err = dev.LoadControl(); err != nil {
			return
		}

		if err = dev.SetBlkMd(blkMdNoop); err != nil {
			return
		}

		if err = dev.repeatLoadControl(4); err != nil {
			return
		}

		if doReset {
			if err = dev.SetBlkMd(blkMdRstBlk); err != nil {
				return
			}

			if err = dev.repeatLoadControl(4); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)

			if err = dev.SetBlkMd(blkMdNoop); err != nil {
				return
			}

			if err = dev.repeatLoadControl(2); err != nil {
				return
			}

			if err = dev.SetGPIOResetComplete(); err != nil {
				return
			}
		}
	} else {
		// loop and clear entire DMD
		for i := uint16(0); i < 16; i++ {
			if err = dev.SetBlkAd(i); err != nil {
				return
			}

			if _, err = dev.LoadControl(); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)

			if err = dev.SetBlkMd(blkMdNoop); err != nil {
				return
			}

			if err = dev.repeatLoadControl(4); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)

			if err = dev.SetBlkMd(blkMdClBlk); err != nil {
				return
			}
		}

		if doReset {
			if err = dev.SetBlkMd(blkMd11); err != nil {
				return
			}

			if err = dev.SetBlkAd(8); err != nil {
				return
			}

			if err = dev.repeatLoadControl(4); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)

			if err = dev.SetBlkMd(blkMdNoop); err != nil {
				return
			}

			if err = dev.repeatLoadControl(2); err != nil {
				return
			}

			if err = dev.SetGPIOResetComplete(); err != nil {
				return
			}
		}
	}

	return
}

// This uses block numbers indexed from 0! The TI C/C++ API uses indexes starting from 1
func (dev *Device) LoadFrame(frameBuffer []byte, blockNum int, doReset bool) (err error) {
	bytesPerBlock := dev.bytesPerRow * dev.rowsPerBlock

	if err = dev.SetBlkMd(blkMdNoop); err != nil {
		return
	}

	if blockNum <= 15 {
		if err = dev.SetRowMd(rowMdSet); err != nil {
			return
		}

		if err = dev.SetRowAddr(uint16(dev.rowsPerBlock * blockNum)); err != nil {
			return
		}

		// 1080p has only 15 blocks is possible that a 16th block could try to be loaded sending data
		// after the FrameBuffer pointer.. added a check to send nothing if block 16 load is requested on the 1080p.
		if !(dev.dmdType == Type1080P && blockNum == 15) {
			start := bytesPerBlock * blockNum
			end := start + dev.bytesPerRow

			if err = dev.LoadData(frameBuffer[start:end]); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)

			if err = dev.SetRowMd(rowMdInc); err != nil {
				return
			}

			start += dev.bytesPerRow
			end = start + (dev.bytesPerRow * (dev.rowsPerBlock - 1))

			if end > len(frameBuffer)-1 {
				end = len(frameBuffer) - 1
			}

			if err = dev.LoadData(frameBuffer[start:end]); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)
		}

		if doReset {
			dev.Reset(blockNum)
		}
	} else {
		// Global load
		if err = dev.SetRowMd(rowMdSetPnt); err != nil {
			return
		}

		if err = dev.SetRowAddr(0); err != nil {
			return
		}

		start := 0
		end := start + dev.bytesPerRow

		if err = dev.LoadData(frameBuffer[start:end]); err != nil {
			return
		}

		time.Sleep(1 * time.Millisecond)

		if err = dev.SetRowMd(rowMdInc); err != nil {
			return
		}

		start = dev.bytesPerRow
		end = start + dev.bytesPerRow*(dev.rowsPerBlock-1)

		if err = dev.LoadData(frameBuffer[start:end]); err != nil {
			return
		}

		time.Sleep(1 * time.Millisecond)

		iMax := (dev.totalBlocks - 1) / dev.blocksPerLoad

		for i := 0; i < iMax; i++ {
			if i == 0 {
				start += dev.bytesPerRow * dev.rowsPerBlock
			} else {
				start += (dev.bytesPerRow * dev.rowsPerBlock) * dev.blocksPerLoad
			}

			end = start + dev.bytesPerRow*dev.rowsPerBlock*dev.blocksPerLoad

			if dev.dmdType == Type1080P {
				end -= 2
			}

			if end > len(frameBuffer)-1 {
				end = len(frameBuffer) - 1
			}

			if err = dev.LoadData(frameBuffer[start:end]); err != nil {
				return
			}

			time.Sleep(1 * time.Millisecond)
		}

		if doReset {
			dev.Reset(16)
		}
	}

	return
}

func (dev *Device) ResetComplete() (resetComplete bool, err error) {
	resetComplete = false

	dev.SetExtResetEnbl(true)

	for {
		var reg uint16

		if reg, err = dev.registerRead(addrResetComplete); err != nil {
			return
		}

		if reg == 1 {
			resetComplete = true
			break
		} else {
			for i := 0; i < 100; i++ {
				if reg, err = dev.registerRead(addrResetComplete); err != nil {
					return
				}

				if reg == 1 {
					resetComplete = true
					break
				}

				time.Sleep(100 * time.Millisecond)
			}
		}
	}

	if err = dev.SetBlkMd(0); err != nil {
		return
	}

	if err = dev.SetBlkAd(0); err != nil {
		return
	}

	if err = dev.repeatLoadControl(2); err != nil {
		return
	}

	if err = dev.SetGPIOResetComplete(); err != nil {
		return
	}

	err = dev.SetExtResetEnbl(false)

	return
}

// This uses block numbers indexed from 0! The TI C/C++ API uses indexes starting from 1
func (dev *Device) Reset(blockNum int) (err error) {
	if err = dev.SetRowMd(rowMdNoop); err != nil {
		return
	}

	if blockNum <= 15 {

		if err = dev.SetBlkAd(uint16(blockNum)); err != nil {
			return
		}

		if err = dev.SetBlkMd(blkMdNoop); err != nil {
			return
		}

		if err = dev.repeatLoadControl(3); err != nil {
			return
		}

		time.Sleep(1 * time.Millisecond)

		if err = dev.SetBlkMd(blkMdRstBlk); err != nil {
			return
		}

		if _, err = dev.LoadControl(); err != nil {
			return
		}

		time.Sleep(1 * time.Millisecond)

		if err = dev.SetBlkMd(blkMdNoop); err != nil {
			return
		}

		if err = dev.repeatLoadControl(3); err != nil {
			return
		}
	} else {
		if err = dev.SetBlkAd(8); err != nil {
			return
		}

		if err = dev.SetBlkMd(blkMd11); err != nil {
			return
		}

		if err = dev.repeatLoadControl(4); err != nil {
			return
		}

		time.Sleep(1 * time.Millisecond)

		if err = dev.SetBlkMd(blkMdNoop); err != nil {
			return
		}

		if err = dev.repeatLoadControl(2); err != nil {
			return
		}
	}

	err = dev.SetGPIOResetComplete()

	return
}
